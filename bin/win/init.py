#HP tools
nuke.pluginAddPath('./hpTools/icons')
nMajor = nuke.NUKE_VERSION_MAJOR
nMinor = nuke.NUKE_VERSION_MINOR
if nMajor == 12 and nMinor == 0:
    nuke.pluginAddPath('./hpTools/Nuke12.0')
if nMajor == 12 and nMinor == 1:
    nuke.pluginAddPath('./hpTools/Nuke12.1')
